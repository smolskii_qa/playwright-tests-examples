# Playwright E2E tests

Here is an example of automation tests using Playwright. 
My education process is still carrying on, so I am confident new commits will bring better code.
Here I wrote a simple test, which tests a typical web form like registration or kind of.

## How to use

Be aware you have installed the Playwright on your machine. Official instructions [here](https://playwright.dev/docs/intro).
Then pull all files from this repository into the playwright folder.
After it, you can use:
```bash
npx playwright test
```
Tests are run in headless mode meaning no browser will open up when running the tests. The results of the tests and test logs will be shown in the terminal.

## Tests logic

First and foremost the it checks the URL of the loaded website. Then each test asserts whether the element (simple form/dropdown/radio button/checkbox) exists, is available, visible, and changeable. It inputs some valid data if it is necessary.

* URL and locators are in '../data/pageLocators.json'

* Data to input is in '../data/data.json'

## Some notes

The tested website is a demo for QA engineers. There are few known bugs, thus some tests will be crashed. Don't be afraid, I'm going to handle these issues in a useful and accurate view later.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)\
Author: Vladislav Smolskii\
[LinkedIn](www.linkedin.com/in/vladislav-smolskii/)