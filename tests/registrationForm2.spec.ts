import { test, expect, type Page } from '@playwright/test'
import data from '../fixtures/data.json'
import pl from '../fixtures/pageLocators.json'

test.describe.configure({ mode: 'serial' });

let page: Page;


test.beforeAll(async ({ browser }) => {
    console.log(browser.version());
    page = await browser.newPage();
});

test.afterAll(async () => {
    await page.close();
});

test('Open the page and check URL', async () => {
    await page.goto(pl.URL);
    expect(page.url()).toBe(pl.URL);
}
);

test('Enter first name', async () => {
    await page.locator(pl.firstName).type(data.fname);
    expect(page.locator(pl.firstName)).toHaveText(data.fname);
    //here und further almsot all forms inputs won't be appaered in html, so the toHaveText check will be failed
}
);

test('Enter last name', async () => {
    await page.locator(pl.lastName).type(data.lname);
    expect(page.locator(pl.lastName)).toHaveText(data.lname);
}
);

test('Enter email', async () => {
    await page.locator(pl.email).type(data.email);
    expect(page.locator(pl.email)).toHaveText(data.email);
}
);

test('Gender radio buttons', async () => {
    for (let key in pl.gender) {
        await page.check(pl.gender[key], { force: true });
        // expect(await page.getByLabel(pl.gender[key]).isChecked()).toBeTruthy();
        // crash
        // check whether it can find byLabel
    };
}
);

test('Enter mobile number', async () => {
    await page.locator(pl.mobileNumber).type(data.mobile);
    expect(page.locator(pl.mobileNumber)).toHaveText(data.mobile);
}
);

// in work!
// test('Enter subjects', async () => {
//     for (let sub in data.subjects) {
//         await page.locator(pl.subjects).click();
//         await page.locator(pl.subjects).type(sub);
//         #subjectsWrapper
//     }
// }
// );


test('Enter birthdate after deleting placeholder', async () => {
    await page.locator(pl.birthDate).click();
    while (await page.locator(pl.subjects).getAttribute('value') != '') {
        await page.keyboard.press('Backspace');
        //the pade crashes when all chars are deleted
        await page.locator(pl.birthDate).type(data.birthDate);
        expect(page.locator(pl.birthDate)).toHaveText(data.birthDate);
    }
}
);


test('Hobbies checkbox', async () => {
    for (let key in pl.hobbies) {
        await page.check(pl.hobbies[key], { force: true })
        expect(await page.getByLabel(pl.hobbies[key]).isChecked()).toBeTruthy();
        // crash
    };

}
);


test('Enter address', async () => {
    await page.locator(pl.address).click();
    for (let key in data.address) {
        await page.locator(pl.address).type(data.address[key]);
        let sep = ', '
        if (data.address[key] == data.address.building) {
            sep = '.'
        };
        await page.locator(pl.address).type(sep);
    };
    expect(page.locator(pl.mobileNumber)).toContainText(data.mobile);
}
);


test('Choose state', async () => {
    await page.locator(pl.state).click()
    // crash, element unavailable bcs of ad banner
    await page.locator(pl.state).selectOption(data.address.state)
}
);

test('Choose city', async () => {
    await page.locator(pl.city).click()
    // crash, element unavailable bcs of ad banner
    await page.locator(pl.city).selectOption(data.address.city)
}
);








// });